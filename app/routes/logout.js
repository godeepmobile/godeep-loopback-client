import Ember from 'ember';

export default Ember.Route.extend({
	model: function() {
		Ember.$.post("http://localhost:3000/api/lb_users/logout").then(function() {
			Ember.$.ajaxSetup({
				headers: {
					Authorization : 'none'
				}
			});
		});
	}
});
