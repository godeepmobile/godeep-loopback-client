import Ember from 'ember';

export default Ember.Route.extend({
	actions: {
        error: function(reason) {
            if (reason.status) {
            	switch (reason.status) {
            		case 401:
            			this.transitionTo('login');
            			break;
            		case 403:
            			this.transitionTo('error');
            			break;
            	}
            }
        }
    }
});
