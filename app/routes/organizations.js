import Ember from 'ember';

export default Ember.Route.extend({
	'model' : function() {
		//return this.store.find('organization');
		return Ember.RSVP.hash({
        	organizations     : this.store.find('organization'),
        	conferences       : this.store.find('conference'),
        	organizationTypes : this.store.find('organizationType')
    	});
	},

	setupController: function (controller, context) {
    	this.controllerFor('organizations').set('model', context.organizations);
    	this.controllerFor('conferences').set('model', context.conferences);
    	this.controllerFor('organizationTypes').set('model', context.organizationTypes);
  	}
});
