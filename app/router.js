import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('organizations', function() {});
  this.resource('organizationTypes', function() {});
  this.resource('conferences', function() {});
  this.route('organization');
  this.route('login');
  this.route('menu');
  this.route('logout');
  this.route('error');
});

export default Router;
