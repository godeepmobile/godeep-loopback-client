import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		create: function() {
			var organization = this.store.createRecord('organization', {
				'name'                    : this.get('name'),
				'short_name'              : this.get('shortName'),
				'espn_name'               : this.get('espnName'),
				'mascot'                  : this.get('mascot'),
				'go_conference_id'        : this.get('selectedConference'),
				'go_organization_type_id' : this.get('selectedOrganizationType')
			}), self = this;

			organization.save().then(function() {
				self.transitionToRoute('menu');
			}).catch(function(reason) {
				if (reason && reason.status === 403) { 
					self.transitionToRoute('error');
				}
			});
		}
	},

	needs: ['conferences', 'organizationTypes']

});
