import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		login: function() {
			var credentials = {
				username : this.get('username'),
				password : this.get('password')
			}, self = this;
			Ember.$.post("http://localhost:3000/api/lb_users/login", credentials).then(function(response) {
				Ember.$.ajaxSetup({
					headers: {
						Authorization : response.id
					}
				});
				self.transitionToRoute('menu');
			}, function() {
				self.set("loginFailed", true);
			});
		}
	}
});
