import DS from 'ember-data';

export default DS.Model.extend({
	'name'                    : DS.attr('string'),
  	'short_name'              : DS.attr('string'),
  	'espn_name'               : DS.attr('string'),
  	'mascot'                  : DS.attr('string'),
  	'go_organization_type_id' : DS.belongsTo('organizationType'),
  	'go_conference_id'        : DS.belongsTo('conference')
});
