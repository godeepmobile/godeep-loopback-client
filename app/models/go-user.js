import DS from 'ember-data';

export default DS.Model.extend({
 	'username' : DS.attr('string'),
  	'realm'    : DS.attr('string'),
  	'lastname' : DS.attr('string')
});
